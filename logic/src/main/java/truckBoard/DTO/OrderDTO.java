package truckBoard.DTO;


public class OrderDTO {

    private Integer id;
    private String start;
    private String finish;
    private String current;
    private String number;
    private String status;
    private String truck;
    private Integer notLoaded;
    private Integer loaded;
    private Integer delivered;
    private Integer estimatedTime;
    private Integer maxLoad;
    private Integer pilotsCount;
    private Integer required;

    public OrderDTO() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTruck() {
        return truck;
    }

    public void setTruck(String truck) {
        this.truck = truck;
    }

    public Integer getEstimatedTime() {
        return estimatedTime;
    }

    public void setEstimatedTime(Integer estimatedTime) {
        this.estimatedTime = estimatedTime;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getFinish() {
        return finish;
    }

    public void setFinish(String finish) {
        this.finish = finish;
    }

    public Integer getMaxLoad() {
        return maxLoad;
    }

    public void setMaxLoad(Integer maxLoad) {
        this.maxLoad = maxLoad;
    }

    public Integer getRequired() {
        return required;
    }

    public void setRequired(Integer required) {
        this.required = required;
    }

    public String getCurrent() {
        return current;
    }

    public void setCurrent(String current) {
        this.current = current;
    }

    public Integer getNotLoaded() {
        return notLoaded;
    }

    public void setNotLoaded(Integer notLoaded) {
        this.notLoaded = notLoaded;
    }

    public Integer getLoaded() {
        return loaded;
    }

    public void setLoaded(Integer loaded) {
        this.loaded = loaded;
    }

    public Integer getDelivered() {
        return delivered;
    }

    public void setDelivered(Integer delivered) {
        this.delivered = delivered;
    }

    public Integer getPilotsCount() {
        return pilotsCount;
    }

    public void setPilotsCount(Integer pilotsCount) {
        this.pilotsCount = pilotsCount;
    }

    @Override
    public String toString() {
        return "OrderDTO{" +
                "id=" + id +
                ", start='" + start + '\'' +
                ", finish='" + finish + '\'' +
                ", current='" + current + '\'' +
                ", number='" + number + '\'' +
                ", status='" + status + '\'' +
                ", truck='" + truck + '\'' +
                ", notLoaded=" + notLoaded +
                ", loaded=" + loaded +
                ", delivered=" + delivered +
                ", estimatedTime=" + estimatedTime +
                ", maxLoad=" + maxLoad +
                ", pilotsCount=" + pilotsCount +
                ", required=" + required +
                '}';
    }
}
