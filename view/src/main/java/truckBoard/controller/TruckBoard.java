package truckBoard.controller;

import truckBoard.DTO.BoardDTO;
import truckBoard.DTO.OrderDTO;
import truckBoard.Service.ModelUpdater;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.push.Push;
import javax.faces.push.PushContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.util.List;


@Named("board")
@ApplicationScoped
public class TruckBoard {

    @Inject
    private ModelUpdater updater;

    @Inject
    @Push
    private PushContext boardChannel;

    private List<OrderDTO> orders;
    private Integer availablePilots;
    private Integer unavailablePilots;
    private Integer freeTrucks;
    private Integer busyTrucks;
    private Integer brokenTrucks;

    @PostConstruct
    public void init() {
        try {
            BoardDTO boardDTO = updater.updateModel();
            this.orders = boardDTO.getOrders();
            this.availablePilots = boardDTO.getAvailablePilots();
            this.unavailablePilots = boardDTO.getUnavailablePilots();
            this.freeTrucks = boardDTO.getFreeTrucks();
            this.busyTrucks = boardDTO.getBusyTrucks();
            this.brokenTrucks = boardDTO.getBrokenTrucks();

        } catch (IOException e) {
            //TODO
            e.printStackTrace();
        }
    }

    public void update() {
        init();
        noticeToUpdate();
    }

    private void noticeToUpdate() {
        boardChannel.send("update");
    }

    public ModelUpdater getUpdater() {
        return updater;
    }

    public void setUpdater(ModelUpdater updater) {
        this.updater = updater;
    }

    public List<OrderDTO> getOrders() {
        return orders;
    }

    public void setOrders(List<OrderDTO> orders) {
        this.orders = orders;
    }

    public Integer getAvailablePilots() {
        return availablePilots;
    }

    public void setAvailablePilots(Integer availablePilots) {
        this.availablePilots = availablePilots;
    }

    public Integer getUnavailablePilots() {
        return unavailablePilots;
    }

    public void setUnavailablePilots(Integer unavailablePilots) {
        this.unavailablePilots = unavailablePilots;
    }

    public Integer getFreeTrucks() {
        return freeTrucks;
    }

    public void setFreeTrucks(Integer freeTrucks) {
        this.freeTrucks = freeTrucks;
    }

    public Integer getBusyTrucks() {
        return busyTrucks;
    }

    public void setBusyTrucks(Integer busyTrucks) {
        this.busyTrucks = busyTrucks;
    }

    public Integer getBrokenTrucks() {
        return brokenTrucks;
    }

    public void setBrokenTrucks(Integer brokenTrucks) {
        this.brokenTrucks = brokenTrucks;
    }
}
