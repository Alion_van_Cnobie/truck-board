package truckBoard.Service;

import com.google.gson.*;
import truckBoard.DTO.BoardDTO;
import truckBoard.DTO.OrderDTO;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class BoardDTODeserializer implements JsonDeserializer<BoardDTO> {
    @Override
    public BoardDTO deserialize(JsonElement json, Type type, JsonDeserializationContext context) throws JsonParseException {

        BoardDTO boardDTO = new BoardDTO();
        JsonObject jsonObject = json.getAsJsonObject();
        boardDTO.setAvailablePilots(jsonObject.get("availablePilots").getAsInt());
        boardDTO.setUnavailablePilots(jsonObject.get("unavailablePilots").getAsInt());
        boardDTO.setFreeTrucks(jsonObject.get("freeTrucks").getAsInt());
        boardDTO.setBusyTrucks(jsonObject.get("busyTrucks").getAsInt());
        boardDTO.setBrokenTrucks(jsonObject.get("brokenTrucks").getAsInt());
        List<OrderDTO> orders = new ArrayList<>();
        boardDTO.setOrders(orders);

        JsonArray jsonOrders = jsonObject.getAsJsonArray("orders");
        for (JsonElement jsonOrder: jsonOrders) {
            orders.add((OrderDTO) context.deserialize(jsonOrder, OrderDTO.class));
        }

        return boardDTO;
    }
}
