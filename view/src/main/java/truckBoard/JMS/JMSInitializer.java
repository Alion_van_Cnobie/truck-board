package truckBoard.JMS;

import truckBoard.controller.TruckBoard;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import javax.ejb.Singleton;
import javax.ejb.Startup;

import javax.inject.Inject;
import javax.jms.*;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import java.util.Properties;
import java.util.logging.Logger;

@Startup
@Singleton
public class JMSInitializer {

    private static final Logger log = Logger.getLogger(JMSInitializer.class.getName());

    private Context jndiContext;
    private Connection jmsConnection;
    private Session jmsSession;
    private MessageConsumer messageConsumer;

    @Inject
    private TruckBoard truckBoard;

    @PostConstruct
    public void init() {

        log.info("JMS Initialization");

        Properties props = new Properties();
        props.put(Context.INITIAL_CONTEXT_FACTORY, "org.apache.activemq.artemis.jndi.ActiveMQInitialContextFactory");
        props.put(Context.PROVIDER_URL, "tcp://localhost:61616");
        props.put("ConnectionFactory", "tcp://localhost:61616");
        props.put("queue.queue/TruckBoard", "TruckBoard");

        try {
            // Look-up remote resources
            jndiContext = new InitialContext(props);
            ConnectionFactory jmsConnectionFactory = (ConnectionFactory) jndiContext.lookup("ConnectionFactory");
            Queue messageQueue = (Queue) jndiContext.lookup("queue/TruckBoard");

            // Connection initialization
            jmsConnection = jmsConnectionFactory.createConnection();
            jmsSession = jmsConnection.createSession(false, Session.AUTO_ACKNOWLEDGE);

            // Create async consumer
            messageConsumer = jmsSession.createConsumer(messageQueue);
            messageConsumer.setMessageListener(new MessageListener() {
                @Override
                public void onMessage(Message message) {
                    try {
                        String msg = message.getBody(String.class);
                        log.info("Message received: " + msg);
                        truckBoard.update();

                    } catch (JMSException e) {
                        log.warning(e.getMessage());
                    }
                }
            });

            jmsConnection.start();

        } catch (NamingException | JMSException e) {

            log.warning("Can't establish JMS connection. " + e.getMessage());

            destroy();
        }
    }


    @PreDestroy
    public void destroy() {

        closeResource(messageConsumer);
        closeResource(jmsSession);
        closeResource(jmsConnection);

        if (jndiContext != null) {
            try {
                jndiContext.close();
            } catch (NamingException e) {
                log.warning(e.getMessage());
            }
        }

    }

    private void closeResource(AutoCloseable resource) {

        if (resource == null) return;

        try {
            resource.close();
        }
        catch (Exception e) {
            log.warning(e.getMessage());
        }
    }

}
